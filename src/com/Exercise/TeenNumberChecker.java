package com.Exercise;

public class TeenNumberChecker {
    public static void main(String[] args) {
                //********* Teen Number Checker ******
        boolean resultTeenNumber = TeenNumberChecker.hasTeen(17,16,7);
        boolean resultIsTeen = TeenNumberChecker.isTeen(7);
        System.out.println("Has Teen: " + resultTeenNumber + "\n" + "Is Teen: " + resultIsTeen);
    }
    public static boolean hasTeen(int number1, int number2, int number3) {
        if (13 > number1 && number1 > 19 || 13 > number2 && number2 > 19 || 13 > number3 && number3 > 19) {
            return false;
        } else if (13 <= number1 && number1 <= 19 || 13 <= number2 && number2 <= 19 || 13 <= number3 && number3 <= 19) {
            return true;
        }
        return false;
    }

    public static boolean isTeen(int teen) {
        if (13 <= teen && teen <= 19) {
            return true;
        } else
            return false;
    }
}
