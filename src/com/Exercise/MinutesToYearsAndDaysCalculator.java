package com.Exercise;

public class MinutesToYearsAndDaysCalculator {
    public static void main(String[] args) {
                //********* Minutes To Years And Days Calculator ******
        MinutesToYearsAndDaysCalculator.printYearsAndDays(561600);
    }
    // public static final String INVALID_VALUE_MASSAGE = "Invalid Value";
    public static void printYearsAndDays(long minutes) {
        if (minutes < 0) {
            System.out.println("Invalid Valve");
        } else {
            long years = minutes / (60 * 24 * 365);
            long remindingYears = minutes % (60 * 24 * 365);
            long days = remindingYears / (60 * 24);
            System.out.println(minutes + " min " + "= " + years + " y and " + days + " d");
        }
    }
}
