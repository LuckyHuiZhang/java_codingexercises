package com.Exercise;

public class NumberToWords {
    public static void main(String[] args) {
                //********* Number In Word ******
        NumberInWord.printNumberInWord(0);
        NumberInWord.printNumberInWord(3);
        NumberInWord.printNumberInWord(6);
        NumberInWord.printNumberInWord(23);
        NumberInWord.printNumberInWord(-12);
    }
    public static void numberToWords(int number) {

        if (number < 0) {
            System.out.println("Invalid Value");
        }

        String reverseNumber = reverse(number);
        for (int i = 0; i < reverseNumber.length(); i++) {

            switch (reverseNumber.charAt(i)) {
                case '0':
                    System.out.print("Zero ");
                    break;
                case '1':
                    System.out.print("One ");
                    break;
                case '2':
                    System.out.print("Two ");
                    break;
                case '3':
                    System.out.print("Three ");
                    break;
                case '4':
                    System.out.print("Four ");
                    break;
                case '5':
                    System.out.print("Five ");
                    break;
                case '6':
                    System.out.print("Six ");
                    break;
                case '7':
                    System.out.print("Seven ");
                    break;
                case '8':
                    System.out.print("Eight ");
                    break;
                case '9':
                    System.out.print("Nine ");
                    break;
                default:
                    break;
            }
        }

        System.out.println();
    }

    public static String reverse(int number) {

        StringBuilder reverseNumber = new StringBuilder();
        while (number != 0) {
            reverseNumber.append(number % 10);
            number /= 10;
        }
        return reverseNumber.toString();
    }

}
