package com.Exercise;

public class GreatestCommonDivisor {
    public static void main(String[] args) {
                //********* Greatest Common Divisor ******
        int divisor = GreatestCommonDivisor.getGreatestCommonDivisor(10,30);
        System.out.println("Greatest Common Divisor:" + divisor);
    }
    public static int getGreatestCommonDivisor(int first, int second) {
        int divisor = 0;
        if (first < 10 || second < 10) {
            return -1;
        }

        for (int i = 1; i <= Math.max(first, second); i++) {
            if (first % i == 0 && second % i == 0) {
                divisor = i;
            }

        }

        return divisor;

    }
}
