package com.Exercise;

public class LeapYearCalculator {
    public static void main(String[] args) {
                //********* LeaP Year Calculator ******
        boolean years = LeapYearCalculator.isLeapYear(1670);
        System.out.println(years);
    }
    public static boolean isLeapYear(int year) {
        if (1 > year || year > 9999) {
            return false;
        }

        if ((year % 4) == 0) {
            if ((year % 100) == 0) {
                if ((year % 400) == 0) {
                    return true;
                } else
                    return false;
            } else
                return true;
        }
        return false;
    }
}
