package com.Exercise;

public class LargestPrime {
    public static void main(String[] args) {
                //********* Largest Prime ******
        int largestPrime = LargestPrime.getLargestPrime(67);
        System.out.println("Largest Prime : " + largestPrime);
    }
    public static int getLargestPrime(int number) {

        if (number <= 1) {
            return -1;

        }
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                number /= i;
                i--;
            }
        }
        return number;
    }
}
