package com.Exercise;

public class LastDigitChecker {
    public static void main(String[] args) {
                //********* Last Digit Checker ******
        boolean lastDigit = LastDigitChecker.hasSameLastDigit(2,52,42);
        System.out.println("Last Digit Checker : " + lastDigit);
        boolean numberCheck = LastDigitChecker.isValid(10);
        System.out.println(numberCheck);

    }
    public static boolean hasSameLastDigit(int num1, int num2, int num3) {
        if (num1 < 10 || num1 > 1000 || num2 < 10 || num2 > 1000 || num3 < 10 || num3 > 1000) {
            return false;
        } else if (num1 % 10 == num2 % 10 || num1 % 10 == num3 % 10 || num2 % 10 == num3 % 10) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isValid(int number) {
        if (number < 10 || number > 1000) {
            return false;
        } else {
            return true;
        }
    }
}
