package com.Exercise;

public class PerfectNumber {
    public static void main(String[] args) {
                //********* Perfect Number ******
        boolean perfectNumber = PerfectNumber.isPerfectNumber(6);
        System.out.println("Perfect Number : " +perfectNumber);
    }
    public static boolean isPerfectNumber(int number) {

        int sum = 0;
        if (number < 1) {
            return false;
        }

        for (int i = 1; i <= number / 2; i++) {
            if (number % i == 0) {
                sum += i;
                System.out.println("Divisors: " + i);
                //System.out.println("Sum: " + sum);
            }

        }
        return sum == number && number > 1;

    }
}
