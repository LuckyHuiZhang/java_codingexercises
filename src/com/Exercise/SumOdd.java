package com.Exercise;

public class SumOdd {
    public static void main(String[] args) {
                //********* Sum Odd ******
        boolean numberOdd = SumOdd.isOdd(13);
        int sumOdd=SumOdd.sumOdd(100,100);
        System.out.println(numberOdd);
        System.out.println(sumOdd);
    }
    public static boolean isOdd(int number) {
        if (number < 0) {
            return false;
        } else return (number % 2 == 1) ? true : false;

    }

    public static int sumOdd(int start, int end) {

        int sum = 0;
        if (end < start || start <= 0) {
            return -1;
        }
        for (int i = start; i <= end; i++) {
            if (isOdd(i)) {
                sum += i;
            }
        }
        return sum;
    }
}
