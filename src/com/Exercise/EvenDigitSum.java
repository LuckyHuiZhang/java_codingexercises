package com.Exercise;

public class EvenDigitSum {
    public static void main(String[] args) {
                //********* Even Digit Sum ******
        int sumOfEvenDigit = EvenDigitSum.getEvenDigitSum(123456789);
        System.out.println(sumOfEvenDigit);
    }
    public static int getEvenDigitSum(int number) {
        if (number < 0) {
            return -1;
        }
        int digit = 0;
        while (number > 0) {

            if (number % 2 == 0) {
                digit += number % 10;
            }
            number /= 10;
        }

        return digit + number;

    }

}
