package com.Exercise;

public class FlourPackProblem {
    public static void main(String[] args) {
                //********* Flour Pack Problem ******
        boolean flourPack = FlourPackProblem.canPack(3,2,2);
        System.out.println("Flour Pack Problem: " + flourPack);
    }
    public static boolean canPack(int bigCount, int smallCount, int goal) {

        int sum = bigCount * 5 + smallCount * 1;
        if (sum < goal) {
            return false;
        } else if (bigCount < 0 || smallCount < 0 || goal < 0) {
            return false;
        } else if (smallCount >= goal) {
            return true;
        }

        int remainderOfBigPack = goal % 5;
        return (smallCount >= remainderOfBigPack);

    }
}
