package com.Exercise;

public class FirstAndLastDigitSum {
    public static void main(String[] args) {
                //********* First And Last Digit Sum ******
        int sumOfFirstAndLastDigit = FirstAndLastDigitSum.sumFirstAndLastDigit(4);
        System.out.println(sumOfFirstAndLastDigit);

    }
    public static int sumFirstAndLastDigit(int number) {

        if (number < 0) {
            return -1;
        }

        int lastDigit = number % 10;
        while (number > 9) {
            number /= 10;
        }

       return lastDigit + number;

    }
}
