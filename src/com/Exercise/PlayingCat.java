package com.Exercise;

public class PlayingCat {
    public static void main(String[] args) {
                //********* Playing Cat ******
        boolean playingCat = PlayingCat.isCatPlaying(true,50);
        System.out.println(playingCat);
    }
    public static boolean isCatPlaying(boolean summer, int temperature) {
        if ((summer == false) && (temperature >= 25 && temperature <= 35)) {
            return true;
        } else if ((summer == true) && (temperature >= 25 && temperature <= 45)) {
            return true;
        } else
            return false;
    }
}
