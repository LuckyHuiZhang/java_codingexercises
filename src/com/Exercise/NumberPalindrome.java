package com.Exercise;

public class NumberPalindrome {
    public static void main(String[] args) {
                //********* Number Palindrome ******
        boolean numberPalindrome = NumberPalindrome.isPalindrome(907);
        System.out.println(numberPalindrome);
    }
    public static boolean isPalindrome(int number) {
        int reverse = 0;
        int origin = number;
        while (number != 0) {
            int lastDigit = number % 10;
            reverse = reverse * 10 + lastDigit;
            number /= 10;

        }
        return reverse == origin;
    }
}
