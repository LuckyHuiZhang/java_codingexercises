package com.Exercise;

public class PaintJob {
    public static void main(String[] args) {
                //********* Paint Job ******
        System.out.println("Paint Job");
        double bucket1 = PaintJob.getBucketCount(-2.75,3.25,2.5,1);
        double bucket2 = PaintJob.getBucketCount(3.4,2.1,1.5);
        double bucket3 = PaintJob.getBucketCount(3.4,1.5);
        System.out.println(bucket1);
        System.out.println(bucket2);
        System.out.println(bucket3);
    }
    public static int getBucketCount(double width, double height, double areaPerBucket, int extraBuckets) {
        if (width <= 0 || height <= 0 || areaPerBucket <= 0 || extraBuckets < 0) {
            return -1;
        } else {
            return (int) Math.ceil((double) width * height / areaPerBucket - extraBuckets);
        }
    }

    public static int getBucketCount(double width, double height, double areaPerBucket) {
        if (width <= 0 || height <= 0 || areaPerBucket <= 0) {
            return -1;
        } else {
            return (int) Math.ceil((double) width * height / areaPerBucket);
        }
    }

    public static int getBucketCount(double area, double areaPerBucket) {
        if (area <= 0 || areaPerBucket <= 0) {
            return -1;
        } else {
            return (int) Math.ceil((double) area / areaPerBucket);
        }
    }

}
