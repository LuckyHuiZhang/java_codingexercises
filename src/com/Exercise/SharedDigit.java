package com.Exercise;

public class SharedDigit {
    public static void main(String[] args) {
                //********* Shared Digit ******
        boolean sharedDigit = SharedDigit.hasSharedDigit(12,93);
        System.out.println(sharedDigit);
    }
    public static boolean hasSharedDigit(int num1, int num2) {
        if (num1 < 10 || num1 > 99 || num2 < 10 || num2 > 99) {
            return false;
        } else {

            return (num1 / 10 == num2 / 10 || num1 / 10 == num2 % 10 || num2 / 10 == num1 % 10 || num1 % 10 == num2 % 10);
        }
    }
}
