package com.InterviewPractice.Stack;

import java.util.Stack;

public class EvaluateReversePolishNotation {
    public static void main(String[] args) {
       // String[] tokens = {"2","1","+","3","*"};
       // String[] tokens = {"4","13","5","/","+"};
        String[] tokens = {"10","6","9","3","+","-11","*","/","*","17","+","5","+"};
        System.out.println(solution(tokens));
    }
    public static int solution(String[] tokens){
        Stack<Integer> s = new Stack<>();
        for (String token : tokens){
            if ("+-*/".contains(token)){
                int y = s.pop();
                int x = s.pop();

                if (token.equals("+"))
                    s.push(x + y);
                else if (token.equals("-"))
                    s.push(x - y);
                else if(token.equals("*"))
                    s.push(x * y);
                else if (token.equals("/"))
                    s.push(x / y);

                }else {
                    s.push(Integer.parseInt(token));
                }
            }

        return s.pop();
    }
}
