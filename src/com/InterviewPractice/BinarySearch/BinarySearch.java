package com.InterviewPractice.BinarySearch;

public class BinarySearch {
    public static void main(String[] args) {
      //  int[] nums = {-1,0,3,5,9,12};
        int[] nums = {-1,0,3,5,9,12};
        System.out.println(solution(nums,2));
    }
    public static int solution(int[] nums, int target){
        if (nums.length == 0){
            return -1;
        }

        int leftPoint = 0;
        int rightPoint = nums.length - 1;

        while (leftPoint <= rightPoint){
            int midPoint = leftPoint + (rightPoint - leftPoint) / 2;
            if (nums[midPoint] == target){
                return midPoint;
            }else if (nums[midPoint] > target){
                rightPoint = midPoint -1;
            }else {
                leftPoint = midPoint +1;
            }
        }
        return -1;
    }
}
