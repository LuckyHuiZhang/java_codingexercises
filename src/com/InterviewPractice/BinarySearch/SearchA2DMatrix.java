package com.InterviewPractice.BinarySearch;

public class SearchA2DMatrix {
    public static void main(String[] args) {
//        int matrix[][] = { { 1,3,5,7 },
//                { 10,11,16,20 },
//                { 23,30,34,60 },};

        int matrix[][] = { { 1,3,5,7 },
                { 10,11,16,20 },
                {23,30,34,60 },};
        System.out.println( searchMatrix(matrix,13));
    }
    public static boolean searchMatrix(int[][] matrix,int target){
        if (matrix.length == 0) return false;

        int rows = matrix.length;
        int columns = matrix[0].length;

        //define index position
        int left = 0;
        int right = rows * columns - 1;

        //implement Binary Search
        while (left <= right){
            int midpoint_index = left + (right-left) / 2;
            int midpoint_value = matrix[midpoint_index/columns][midpoint_index%columns];
            if (midpoint_value == target) {
                return true;
            }else if (target < midpoint_value){
                right = midpoint_index - 1;
            } else if (target > midpoint_index){
                left = midpoint_index + 1;
            }

        }
        return false;

    }
}
