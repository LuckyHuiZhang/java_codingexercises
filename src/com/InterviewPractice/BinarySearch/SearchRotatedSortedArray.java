package com.InterviewPractice.BinarySearch;

public class SearchRotatedSortedArray {
    public static void main(String[] args) {
        int[] nums = {4,5,6,7,0,1,2};
        System.out.println(solution(nums, 0));
    }
    public static int solution(int[] nums, int target){
        if (nums == null || nums.length == 0){
            return -1;
        }

        int left_pointer = 0;
        int right_pointer = nums.length - 1;

        while (left_pointer < right_pointer){
            int mid_pointer = left_pointer + (right_pointer-left_pointer) / 2 ;
            if (nums[mid_pointer] > nums[right_pointer]){
                left_pointer = mid_pointer + 1;
            }else {
                right_pointer = mid_pointer;
            }
        }
        int new_start = left_pointer;
        left_pointer = 0;
        right_pointer = nums.length - 1;

        if (target >= nums[new_start] && target <= nums[right_pointer]){
            left_pointer = new_start;
        }else {
            right_pointer = new_start;
        }

        while (left_pointer <= right_pointer){
            int mid_pointer = left_pointer + (right_pointer -left_pointer) / 2;
            if (nums[mid_pointer] == target){
                return mid_pointer;
            }else if (nums[mid_pointer] < target){
                left_pointer = mid_pointer + 1;
            }else {
                right_pointer = mid_pointer - 1;
            }
        }
        return -1;
    }
}
