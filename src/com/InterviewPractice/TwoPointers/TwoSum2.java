package com.InterviewPractice.TwoPointers;

import java.util.Arrays;

public class TwoSum2 {
    public static void main(String[] args) {
        int[] numbers = {2,3,4};
        //int[] numbers = {2,7,11,15};
        System.out.println(Arrays.toString(solution(numbers,6)));
    }
    public static int[] solution(int[] numbers, int target){
        int left_pointer = 0;
        int right_pointer = numbers.length - 1;

        while (left_pointer <= right_pointer){
            int sum = numbers[left_pointer] + numbers[right_pointer];

            if (sum > target) {
                right_pointer -= 1;
            } else if (sum < target){
                left_pointer += 1;
            } else {
                return new int[] {left_pointer + 1, right_pointer + 1};
            }
        }
        return new int[] {left_pointer + 1, right_pointer + 1};
    }
}
