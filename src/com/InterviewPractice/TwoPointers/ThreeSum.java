package com.InterviewPractice.TwoPointers;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ThreeSum {
    public static void main(String[] args) {
        int[] nums = {-1,0,1,2,-1,-4};
        System.out.println(solution(nums));
    }
    public static List<List<Integer>> solution(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> output_arr = new LinkedList<>();

        for (int i=0; i<nums.length-2; i++){
            if (i == 0 || (i > 0 && nums[i] != nums[i-1])){
                int left_pointer = i+1;
                int right_pointer = nums.length-1;
                int sum = 0-nums[i];

                while (left_pointer < right_pointer) {
                    if (nums[left_pointer] + nums[right_pointer] == sum) {

                        output_arr.add(Arrays.asList(nums[i], nums[left_pointer], nums[right_pointer]));

                        while (left_pointer < right_pointer && nums[left_pointer] == nums[left_pointer + 1]) {
                            left_pointer++;
                        }
                        while (left_pointer < right_pointer && nums[right_pointer] == nums[right_pointer - 1]) {
                            right_pointer--;
                        }
                        left_pointer++;
                        right_pointer--;
                    }else if(nums[left_pointer] + nums[right_pointer] > sum){
                        right_pointer--;
                    }else{
                        left_pointer++;
                    }
                }
            }
        }
        return output_arr;
    }
}
