package com.InterviewPractice.TwoPointers;

public class ContainerWithMostWater {
    public static void main(String[] args) {
        int[] height = {1,8,6,2,5,4,8,3,7};
        System.out.println(solution(height));
    }
    public static int solution(int[] height){
        int max_area = 0;
        int left_pointer = 0;
        int right_pointer = height.length - 1;

        while (left_pointer < right_pointer){

            if (height[left_pointer] < height[right_pointer]){
                max_area = Math.max(max_area, height[left_pointer] * (right_pointer-left_pointer));
                left_pointer += 1;
            }else {
                max_area = Math.max(max_area, height[right_pointer] * (right_pointer-left_pointer));
                right_pointer -= 1;

            }
        }
        return max_area;
    }
}
