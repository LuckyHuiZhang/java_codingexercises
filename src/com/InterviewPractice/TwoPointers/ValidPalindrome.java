package com.InterviewPractice.TwoPointers;

public class ValidPalindrome {
    public static void main(String[] args) {
        String result = "A man, a plan, a canal: Panama";
        System.out.println(isPalindrome(result));
    }
    public static boolean isPalindrome(String s){
        String fixed_string = "";
        for (char c : s.toCharArray()){
            if (Character.isDigit(c) || Character.isLetter(c)){
                fixed_string += c;
            }
        }
        fixed_string = fixed_string.toLowerCase();

        int left_pointer = 0;
        int right_pointer = fixed_string.length()-1;

        while (left_pointer <= right_pointer){
            if (fixed_string.charAt(left_pointer) != fixed_string.charAt(right_pointer)){
                return false;
            }
            left_pointer += 1;
            right_pointer -= 1;
        }
        return true;
    }

}
