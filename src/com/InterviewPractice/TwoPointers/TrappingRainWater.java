package com.InterviewPractice.TwoPointers;

public class TrappingRainWater {
    public static void main(String[] args) {
        int[] height = {4,2,0,3,2,5};
        System.out.println(solution(height));
    }
    public static int solution(int[] height){
        int result = 0;
        int level = 0;
        int left_pointer = 0;
        int right_pointer = height.length - 1;

        if (height == null || height.length == 0){
            return 0;
        }

        while (left_pointer < right_pointer){

            int lower = height[height[left_pointer] < height[right_pointer] ? left_pointer++ : right_pointer--];
            level = Math.max(level, lower);
            result += level - lower;
        }
        return result;
    }
}
