package com.InterviewPractice.LinkedList;


public class ReverseLinkedList {
    public static void main(String[] args) {
//        ListNode head = new ListNode(1);
//        head.next = new ListNode(2);
//        head.next.next = new ListNode(3);
//        head.next.next.next = new ListNode(4);
//        head.next.next.next.next = new ListNode(5);
//
//        ListNode result = solution(head);
//        while (result != null){
//            System.out.println(result.val);
//            result = result.next;
//        }

    }
//    public static ListNode solution(ListNode head){
//        ListNode previous = null;
//
//        while (head != null){
//            ListNode next_node = head.next;
//            head.next = previous;
//            previous = head;
//            head = next_node;
//        }
//        return previous;
//    }

    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }
}
