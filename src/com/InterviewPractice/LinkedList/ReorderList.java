package com.InterviewPractice.LinkedList;

public class ReorderList {
    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);

//        ListNode result = solution(head);
//        while (result != null){
//            System.out.println(result.val);
//            result = result.next;
//        }
    }
    public static void solution(ListNode head){
        if (head == null || head.next == null) return;

        //first half head
        ListNode l1 = head;
        //second half head
        ListNode slow = head;
        //second half tail
        ListNode fast = head;
        //first half tail
        ListNode prev = null;

        while (fast != null && fast.next != null){
            prev = slow;
            slow = slow.next;
            fast = fast.next.next;
        }
        prev.next = null;
        ListNode l2 = reverse(slow);
        merge(l1, l2);
    }

    public static ListNode reverse(ListNode head){
        ListNode prev = null;
        ListNode current_node = head;

        while (current_node != null){
            ListNode next_node =  current_node.next;
            current_node.next = prev;
            prev = current_node;
            current_node = next_node;
        }
        return prev;

    }

    public static void merge(ListNode l1, ListNode l2){
        while (l1 != null){
            ListNode l1_next = l1.next;
            ListNode l2_next = l2.next;

            l1.next = l2;

            if (l1_next == null){
                break;
            }
            l2.next = l1_next;
            l1 = l1_next;
            l2 = l2_next;

        }
    }

      public static class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }
}
