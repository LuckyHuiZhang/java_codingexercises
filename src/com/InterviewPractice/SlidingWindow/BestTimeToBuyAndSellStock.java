package com.InterviewPractice.SlidingWindow;

public class BestTimeToBuyAndSellStock {
    public static void main(String[] args) {
       // int [] prices= {7,1,5,3,6,4};
        int [] prices= {7,6,4,3,1};
        System.out.println(solution(prices));
    }
    public static int solution(int[] prices){
        int min_cost = Integer.MAX_VALUE;
        int max_profit = 0;

        for (int i=0; i<prices.length; i++){
            if (prices[i] < min_cost){
                min_cost = prices[i];
            }else if (prices[i] - min_cost > max_profit){
                max_profit = prices[i] - min_cost;
            }
        }
        return max_profit;
    }
}
