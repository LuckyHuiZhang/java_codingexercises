package com.InterviewPractice.HeapAndPriorityQueue;

import java.util.PriorityQueue;

public class LastStoneWeight {
    public static void main(String[] args) {
        int[] stones= {2,7,4,1,8,1};
        System.out.println(solution(stones));
    }
    public static int solution(int[] stones){
        PriorityQueue<Integer> heap = new PriorityQueue<>((a,b) -> b-a);

        for (int stone : stones){
            heap.add(stone);
        }

        while (heap.size() > 1){
            int stone1 = heap.remove();
            int stone2 = heap.remove();
            if (stone1 != stone2){
                heap.add(stone1 - stone2);
            }
        }
        return heap.isEmpty() ? 0 :heap.remove();
    }
}
