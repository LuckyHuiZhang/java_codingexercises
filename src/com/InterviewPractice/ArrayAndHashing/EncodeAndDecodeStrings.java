package com.InterviewPractice.ArrayAndHashing;

import java.util.ArrayList;
import java.util.List;

public class EncodeAndDecodeStrings {
    public static void main(String[] args) {
        List<String> strs = new ArrayList<>(List.of("lint" , "code", "love", "love"));
        String[] str= {"lint" , "code", "love", "love"};
        System.out.println(encode(List.of(str)));
        System.out.println(decode(String.valueOf(strs)));

    }

    public static String encode(List<String> strs){
        StringBuilder encodeString = new StringBuilder();
        for (String str : strs){
            int length = str.length();
            encodeString.append(length + "#");
            encodeString.append(str);
        }
        return encodeString.toString();
    }

    public static List<String> decode(String str){
        List<String> decodeString = new ArrayList<>();
        for (int i=0; i<str.length(); i++){
            String length = "";
            while (str.charAt(i) != '#'){
                length += str.charAt(i);
                i++;

            }
            int wordLength = Integer.parseInt(length);
            i++;

            String word = "";
            for (int j=i; j<wordLength+1; j++){
                word += str.charAt(j);
            }
            decodeString.add(word);
            i = i + wordLength - 1;
        }
        return decodeString;
    }
}
