package com.InterviewPractice.ArrayAndHashing;

import java.util.HashSet;

public class LongestConsecutiveSequence {
    public static void main(String[] args) {
        int[] nums = {100,4,200,1,3,2};
        System.out.println(solution(nums));
    }
    public static int solution(int[] nums) {
        HashSet<Integer> num_set = new HashSet<>();

        for (int num : nums) {
            num_set.add(num);
        }
        int longest_sequence_length = 0;

        for (int i = 0; i < nums.length; i++) {
            int current_num = nums[i];
            int current_sequence_length = 1;

            if (!num_set.contains(current_num - 1)) {
                while (num_set.contains(current_num + 1)) {
                    current_num += 1;
                    current_sequence_length += 1;
                }

                longest_sequence_length = Math.max(longest_sequence_length, current_sequence_length);
            }
        }
        return longest_sequence_length;
    }
}
