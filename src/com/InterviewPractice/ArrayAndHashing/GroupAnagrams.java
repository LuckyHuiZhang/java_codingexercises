package com.InterviewPractice.ArrayAndHashing;

import java.util.*;

public class GroupAnagrams {
    public static void main(String[] args) {
        String[] anagrams = new String[] {"eat", "tea", "tan", "ate", "nat", "bat"};
        List<List<String>> result = groupAnagrams(anagrams);
        for(List<String> list : result) {
            for(String str : list) {
                System.out.print(str + ", ");
            }
            System.out.println();
        }
    }
    public static List<List<String>> groupAnagrams(String[] strs) {

        String temp;
        char[] array;
        List<String> list;
// Create the HashMap, object of type String as a Key and List of Strings as a value
        Map<String, List<String>> map = new HashMap<>();
// Traverse the input list of strings from start to end and for each String S,
        for(String str: strs){
// convert it into a character array
            array = str.toCharArray();
            //  Sort the character array created in the last step and convert it into a string
            Arrays.sort(array);
            temp = String.valueOf(array);
//  Put the sorted string as a key and List of Strings (after adding String S) as a value into the HashMap
            if(map.containsKey(temp)){
                map.get(temp).add(str);
            }else {
                list = new ArrayList<>();
                list.add(str);
                map.put(temp, list);
            }

        }
// Return the list of values of HashMap as a result
        List<List<String>> result = new ArrayList<>();
        for(List<String> value: map.values()){
            result.add(value);
        }
        return result;
    }
}
