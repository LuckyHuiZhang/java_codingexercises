package com.InterviewPractice.ArrayAndHashing;

import java.util.HashSet;

public class ContainsDuplicate {
    public static void main(String[] args) {
//        int[] nums = {1,2,3,1};
        int[] nums = {1,2,3,4};
        System.out.println(solution(nums));
    }
    public static boolean solution(int[] nums){
        HashSet<Integer> numbers = new HashSet<>();

        for (int i=0; i<nums.length; i++){
            if (numbers.contains(nums[i])){
                return true;
            }
            numbers.add(nums[i]);
        }
        return false;
    }
}
