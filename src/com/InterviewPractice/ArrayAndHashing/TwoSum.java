package com.InterviewPractice.ArrayAndHashing;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {
    public static void main(String[] args) {
        int[] nums= {2,7,11,15};
        System.out.println(Arrays.toString(solution(nums,9)));

    }
    public static int[] solution(int[] nums, int target){
        Map<Integer, Integer> num_map = new HashMap<>();

        for (int i=0; i<nums.length; i++){
            int findNum = target - nums[i];
            if (num_map.containsKey(findNum)){
                return new int[] {num_map.get(findNum),i };
            }
            num_map.put(nums[i],i);
        }
        throw new IllegalArgumentException("NO match found!");
    }
}
