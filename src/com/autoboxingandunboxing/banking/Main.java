package com.autoboxingandunboxing.banking;

public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank("National Canada Bank");

        if (bank.addBranch("Montreal")) {
            System.out.println("Montreal branch created");
        }

        bank.addCustomer("Montreal", "Lucky", 234.12);
        bank.addCustomer("Montreal", "Jason", 654.84);


        bank.addBranch("Toronto");
        bank.addCustomer("Toronto", "Lena", 65.98);
        bank.addCustomer("Toronto", "Vicky", 12.45);

        bank.addCustomerTransaction("Montreal", "Lucky", 34.55);
        bank.addCustomerTransaction("Toronto", "Lena", 236.80);

        bank.listCustomers("Montreal", true);
        bank.listCustomers("Toronto", true);

        bank.addBranch("Vancouver");

        //Validation check
        if (!bank.addCustomer("Vancouver", "Hellen", 765.23)) {
            System.out.println("Error Vancouver branch does not exist");
        }

        if (!bank.addBranch("Montreal")) {
            System.out.println("Montreal branch already exists");
        }

        if (!bank.addCustomerTransaction("Montreal", "Siri", 12.65)) {
            System.out.println("Customer does not exist at branch");
        }

        if (!bank.addCustomer("Montreal", "Lucky", 231.76)) {
            System.out.println("Customer Lucky already exists");
        }
    }


}
