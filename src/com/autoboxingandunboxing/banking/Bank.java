package com.autoboxingandunboxing.banking;

import java.util.ArrayList;

public class Bank {
    private String name;
    private ArrayList<Branch> branches;

    public Bank(String name) {
        this.name = name;
        this.branches = new ArrayList<Branch>();
    }

    private Branch findBranch(String nameOfBranch) {
        for (int i = 0; i < this.branches.size(); i++) {
            if (this.branches.get(i).getName().equals(nameOfBranch)) {
                return this.branches.get(i);
            }
        }
        return null;
    }

    public boolean addBranch(String nameOfBranch) {
        Branch newBranch = findBranch(nameOfBranch);
        if (newBranch == null) {
            newBranch = new Branch(nameOfBranch);
            this.branches.add(newBranch);

            return true;
        } else {
            return false;
        }
    }

    public boolean addCustomer(String nameOfBranch, String nameOfCustomer, double transaction) {
        Branch newBranch = findBranch(nameOfBranch);
        if (newBranch != null) {
            return newBranch.addNewCustomer(nameOfCustomer, transaction);
        } else {
            return false;

        }
    }

    public boolean addCustomerTransaction(String nameOfBranch, String nameOfCustomer, double transaction) {
        Branch newBranch = findBranch(nameOfBranch);
        if (newBranch != null) {
            return newBranch.addCustomerTransaction(nameOfCustomer, transaction);
        } else {
            return false;

        }
    }

    public boolean listCustomers(String nameOfBranch, boolean printTransactions) {
        Branch newBranch = findBranch(nameOfBranch);
        if (newBranch != null && printTransactions) {
            System.out.println("Customer details for branch " + newBranch.getName());
            for (int i = 0; i < newBranch.getCustomers().size(); i++) {
                System.out.println("Customer: " + newBranch.getCustomers().get(i).getName() + "[" + (i + 1) + "]");
                System.out.println("Transactions");
                for (int j = 0; j < newBranch.getCustomers().get(i).getTransactions().size(); j++) {
                    System.out.println("[" + (j + 1) + "]  Amount " + newBranch.getCustomers().get(i).getTransactions().get(j));
                }
            }
            return true;
        } else if (newBranch != null) {
            System.out.println("Customer details for branch " + newBranch.getName());
            for (int i = 0; i < newBranch.getCustomers().size(); i++) {
                System.out.println("Customer: " + newBranch.getCustomers().get(i).getName() + "[" + (i + 1) + "]");
            }
            return true;
        } else {
            return false;
        }
    }


}


