package com.autoboxingandunboxing.banking;

import java.util.ArrayList;

public class Branch {
    private String name;
    private ArrayList<Customer> customers;

    public Branch(String name) {
        this.name = name;
        this.customers = new ArrayList<Customer>();
    }

    public String getName() {
        return name;
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    private Customer findCustomer(String nameOfCustomer) {
        for (int i = 0; i < this.customers.size(); i++) {
            if (this.customers.get(i).getName().equals(nameOfCustomer)) {
                return this.customers.get(i);
            }
        }

        return null;
    }

    public boolean addNewCustomer(String nameOfCustomer, double initialTransaction) {
        Customer newCustomer = findCustomer(nameOfCustomer);
        if (newCustomer == null) {
            newCustomer = new Customer(nameOfCustomer, initialTransaction);
            this.customers.add(newCustomer);
            return true;
        } else {
            return false;
        }
    }

    public boolean addCustomerTransaction(String nameOfCustomer, double transaction) {
        Customer newCustomer = findCustomer(nameOfCustomer);
        if (newCustomer != null) {
            newCustomer.addTransaction(transaction);
            return true;
        } else {
            return false;
        }
    }

}

