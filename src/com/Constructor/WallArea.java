package com.Constructor;

public class WallArea {
    public static void main(String[] args) {
//        ********* Wall Area ******
        System.out.println("Wall Area");
        WallArea wall = new WallArea();
        wall.setHeight(4);
        wall.setWidth(5);
        System.out.println("Height: " + wall.getHeight());
        System.out.println("Width: " + wall.getWidth());
        System.out.println("Area: " + wall.getArea());

    }

    private double width;
    private double height;

    public WallArea() {
        width = 0;
        height = 0;
    }

    public WallArea(double width, double height) {
        this.width = width < 0 ? 0 : width;
        this.height = height < 0 ? 0 : height;

    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public void setWidth(double width) {
        this.width = width;
        if (width < 0) {
            this.width = 0;
        } else
            this.width = width;
    }

    public void setHeight(double height) {

        if (height < 0) {
            this.height = 0;
        } else
            this.height = height;
    }

    public double getArea() {
        return width * height;
    }
}
