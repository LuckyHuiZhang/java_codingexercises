package com.linkedList.songplaylist;

import java.util.*;

public class Main {
    private static ArrayList<Album> albums = new ArrayList<Album>();

    public static void main(String[] args) {
        Album album = new Album("Album 1", "Artist 1");
        album.addSong("Album1-Song 1", 5.27);
        album.addSong("Album1-Song 2", 4.42);
        album.addSong("Album1-Song 3", 5.33);
        album.addSong("Album1-Song 4", 4.17);
        album.addSong("Album1-Song 5", 5.43);
        album.addSong("Album1-Song 6", 3.54);
        album.addSong("Album1-Song 7", 5.12);
        album.addSong("Album1-Song 8", 5.42);
        albums.add(album);

        album = new Album("Album 2", "Artist 2");
        album.addSong("Album2-Song 1", 5.27);
        album.addSong("Album2-Song 2", 4.42);
        album.addSong("Album2-Song 3", 5.33);
        album.addSong("Album2-Song 4", 4.17);
        album.addSong("Album2-Song 5", 5.43);
        album.addSong("Album2-Song 6", 3.54);
        album.addSong("Album2-Song 7", 5.12);
        album.addSong("Album2-Song 8", 5.42);
        albums.add(album);

        LinkedList<Song> playList = new LinkedList<Song>();
        albums.get(0).addToPlayList("Album1-Song 3", playList);
        albums.get(0).addToPlayList("Album1-Song 5", playList);
        albums.get(0).addToPlayList("Album1-Song 10", playList);
        albums.get(0).addToPlayList(1, playList);
        albums.get(0).addToPlayList(4, playList);
        albums.get(1).addToPlayList(6, playList);
        albums.get(1).addToPlayList(8, playList);
        albums.get(1).addToPlayList(15, playList);

        play(playList);
    }

    private static void play(LinkedList<Song> playList) {
        Scanner scanner = new Scanner(System.in);

        boolean quit = false;
        boolean forward = true;
        ListIterator<Song> listIterator = playList.listIterator();

        if (playList.size() == 0) {
            System.out.println("No songs in playlist");
            return;
        } else {
            System.out.println("Now playing -> " + listIterator.next().toString());
            printMenu();
        }

        while (!quit) {
            int action = scanner.nextInt();
            scanner.nextLine();

            switch (action) {
                case 0:
                    System.out.println("Playlist complete.");
                    quit = true;
                    break;
                case 1:
                    if (!forward) {
                        if (listIterator.hasNext()) {
                            listIterator.next();
                        }
                        forward = true;
                    }
                    if (listIterator.hasNext()) {
                        System.out.println("Now playing -> " + listIterator.next().toString());
                    } else {
                        System.out.println("We have reached the end of the playlist");
                        forward = false;
                    }
                    break;

                case 2:
                    if (forward) {
                        if (listIterator.hasPrevious()) {
                            listIterator.previous();
                        }
                        forward = false;
                    }
                    if (listIterator.hasPrevious()) {
                        System.out.println("Now playing -> " + listIterator.previous().toString());
                    } else {
                        System.out.println("We are at the start of the playlist");
                        forward = true;
                    }
                    break;
                case 3:
                    if (forward) {
                        if (listIterator.hasPrevious()) {
                            System.out.println("Now replaying -> " + listIterator.previous().toString());
                            forward = false;
                        } else {
                            System.out.println("We are at the start of the list");
                        }
                    } else {
                        if (listIterator.hasNext()) {
                            System.out.println("Now replaying -> " + listIterator.next().toString());
                            forward = true;
                        } else {
                            System.out.println("We have reached the end of the list");
                        }
                    }
                    break;

                case 4:

                case 5:
                    printList(playList);
                    break;
                case 6:
                    printMenu();
                    break;

                case 7:
                    if (playList.size() > 0) {
                        listIterator.remove();
                        if (listIterator.hasNext()) {
                            System.out.println("Now playing -> " + listIterator.next());
                        } else if (listIterator.hasPrevious()) {
                            System.out.println("Now playing -> " + listIterator.previous());
                        }
                    }
                    break;

            }
        }
    }

    private static void printMenu() {
        System.out.println("Available actions:\npress");
        System.out.println("0 - to quit\n" +
                "1 - to play next song\n" +
                "2 - to play previous song\n" +
                "3 - to replay the current song\n" +
                "4 - list songs in the playlist\n" +
                "5 - print available actions.\n" +
                "6 - delete current song from playlist");

    }

    private static void printList(LinkedList<Song> playList) {
        Iterator<Song> iterator = playList.iterator();
        System.out.println("================================");
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("================================");
    }
}
