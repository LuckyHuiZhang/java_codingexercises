package com.abstracts.linkedtree;

import com.abstracts.linkedtree.ListItem;

public interface NodeList {
    ListItem getRoot();

    boolean addItem(ListItem item);

    boolean removeItem(ListItem item);

    void traverse(ListItem root);
}
