package com.array;

import java.util.Arrays;
import java.util.Scanner;

public class MinimumElement {
   // public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        MinimumElement minimumElement = new MinimumElement();
        int[] myElement = minimumElement.readElements(3);
        int min = minimumElement.findMin(myElement);
        System.out.println("The minimum element is : " + min);
    }

    public int readInteger() {
        Scanner scanner = new Scanner(System.in);
        //System.out.printf("Enter Count:");
        int capacity = scanner.nextInt();
        // scanner.close();
        return capacity;
    }


    public int[] readElements(int capacity) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[capacity];

        for (int i = 0; i < array.length; i++) {
            System.out.printf("Enter %d integer value:", i);
            array[i] = scanner.nextInt();
        }
        scanner.close();
        return array;

    }

    public int findMin(int[] array) {
        int[] count = Arrays.copyOf(array, array.length);

        boolean flag = true;
        int min;


        while (flag) {

            flag = false;
            for (int i = 0; i < count.length - 1; i++) {
                if (count[i] > count[i + 1]) {
                    min = count[i];
                    count[i] = count[i + 1];
                    count[i + 1] = min;
                    flag = true;
                }
            }
        }
        min = count[0];
        return min;
    }


}
