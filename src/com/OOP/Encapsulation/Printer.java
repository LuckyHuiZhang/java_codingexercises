package com.OOP.Encapsulation;

public class Printer{
    public static void main(String[] args) {
        Printer printer = new Printer(50, true);
        System.out.println(printer.addToner(50));
        System.out.println(printer.getPagesPrinted());
        int pagesPrinted1 = printer.printPages(4);
        System.out.println("Pages Printed was: " + pagesPrinted1 + "\tTotal count: " +printer.getPagesPrinted());
        int pagesPrinted2 = printer.printPages(2);
        System.out.println("Pages Printed was: " + pagesPrinted2 +  "\tTotal count: " +printer.getPagesPrinted());
    }

    private int tonerLevel;
    private int pagesPrinted;
    private boolean duplex;

    public Printer(int tonerLevel, boolean duplex){
        this.tonerLevel = (-1 < tonerLevel && tonerLevel <= 100) ? tonerLevel : -1;
        this.duplex = duplex;
        this.pagesPrinted = 0;
    }

    public int addToner(int tonerAmount){
        if (0< tonerAmount && tonerAmount <= 100){
            return (tonerLevel + tonerAmount > 100) ? -1 : (tonerLevel += tonerAmount);
        }
        return -1;
    }

    public int printPages(int pages){
        int pagesToPrint = (duplex) ? (Math.round((float) pages/2)) : pages;
        pagesPrinted += pagesToPrint;
        return pagesToPrint;

    }

    public int getPagesPrinted(){
        return pagesPrinted;
    }

}
