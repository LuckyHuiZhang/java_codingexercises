package com.OOP.billsburger;

public class HealthBurger extends Hamburger {
    private String healthyExtra1Name;
    private String healthyExtra2Name;

    private double healthyExtra1Price;
    private double healthyExtra2Price;

    public HealthBurger(String meat, double price) {
        super("Health Burger", meat, price, "Sesame");


    }

    public void addHealthyAddition1(String name, double price) {
        this.healthyExtra1Name = name;
        this.healthyExtra1Price += price;
    }

    public void addHealthyAddition2(String name, double price) {
        this.healthyExtra2Name = name;
        this.healthyExtra2Price += price;
    }

    @Override
    public double itemizeHamburger() {
        double totalPrice = super.price + healthyExtra1Price + healthyExtra2Price;
        System.out.println("Order Details: Order of one Health Burger on sesame bread with " +
                healthyExtra1Name + " at " + healthyExtra1Price + "$," +
                " and " + healthyExtra2Name + " at " + healthyExtra1Price + "$,");
        return totalPrice;
    }

}
