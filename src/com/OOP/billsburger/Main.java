package com.OOP.billsburger;

public class Main {
    public static void main(String[] args) {

        Hamburger hamburger = new Hamburger("Cheese Burger", "Beef", 5.45, "White");
        hamburger.addHamburgerAddition1("Tomato", 0.45);
        hamburger.addHamburgerAddition2("Lettuce", 1.25);
        System.out.println("Order Price: " + hamburger.itemizeHamburger());

        DeluxeBurger deluxeBurger = new DeluxeBurger();
        deluxeBurger.addHamburgerAddition1("Bacon", 2.45);
        System.out.println("Order Price: " + deluxeBurger.itemizeHamburger());

        HealthBurger healthBurger = new HealthBurger("Beyond Meat", 16.45);
        healthBurger.addHealthyAddition1("Tomato", 0.45);
        healthBurger.addHealthyAddition2("Lettuce", 1.25);
        System.out.println("Order Price: " + healthBurger.itemizeHamburger());

    }

}
