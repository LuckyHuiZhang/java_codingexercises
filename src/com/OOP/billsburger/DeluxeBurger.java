package com.OOP.billsburger;

public class DeluxeBurger extends Hamburger {
    public DeluxeBurger() {
        super("Deluxe", "Beef & chicken", 18.45, "White");
        super.addHamburgerAddition1("Chips", 2.45);
        super.addHamburgerAddition2("Drink", 1.45);
    }

    @Override
    public void addHamburgerAddition1(String name, double price) {
        System.out.println("No additional items can add");
    }

    @Override
    public void addHamburgerAddition2(String name, double price) {
        System.out.println("No additional items can add");
    }

    @Override
    public void addHamburgerAddition3(String name, double price) {
        System.out.println("No additional items can add");
    }

    @Override
    public void addHamburgerAddition4(String name, double price) {
        System.out.println("No additional items can add");
    }
}
