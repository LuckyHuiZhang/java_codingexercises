package com.OOP.Inheritance;

public class Vehicles {
    public static void main(String[] args) {
        Car car = new Car(8, "Base Car");
        System.out.println(car.startEngine());
        System.out.println(car.accelerate());
        System.out.println(car.brake());
        Mitsubishi mitsubishi =  new Mitsubishi(6, "Outlander VRX 4WD");
        System.out.println(mitsubishi.startEngine());
        System.out.println(mitsubishi.accelerate());
        System.out.println(mitsubishi.brake());
        Ford ford = new Ford(6, "Ford Falcon");
        System.out.println(ford.startEngine());
        System.out.println(ford.accelerate());
        System.out.println(ford.brake());
        Holden holden = new  Holden(6, "Holden Commodore");
        System.out.println(holden.startEngine());
        System.out.println(holden.accelerate());
        System.out.println(holden.brake());
    }

    public static class Car{
        private boolean engine;
        private int cylinders;
        private String name;
        private int wheels;

        public Car(int cylinders, String name){
            this.cylinders = cylinders;
            this.name = name;
            this.engine = true;
            this.wheels = 4;
        }

        public String startEngine()
        {
            String message = " engine is starting! ";
            return getClass().getSimpleName() + message;
        }

        public String accelerate(){
            String message = " engine is accelerating! ";
            return getClass().getSimpleName() + message;
        }

        public String brake(){
            String message = " engine is braking! ";
            return getClass().getSimpleName() + message;
        }

        public int getCylinders(){
            return cylinders;
        }

        public String getName(){
            return name;
        }
    }

    public static class Mitsubishi extends Car{
        public Mitsubishi(int cylinders, String name){
            super(cylinders, name);
        }

        @Override
        public String startEngine() {
            return super.startEngine();
        }

        @Override
        public String accelerate() {
            return super.accelerate();
        }

        @Override
        public String brake() {
            return super.brake();
        }
    }

    public static class Holden extends Car{
        public Holden(int cylinders, String name){
            super(cylinders, name);
        }

        @Override
        public String startEngine() {
            return super.startEngine();
        }

        @Override
        public String accelerate() {
            return super.accelerate();
        }

        @Override
        public String brake() {
            return super.brake();
        }

    }

    public static class Ford extends Car{
        public Ford(int cylinders, String name){
            super(cylinders, name);
        }

        @Override
        public String startEngine() {
            return super.startEngine();
        }

        @Override
        public String accelerate() {
            return super.accelerate();
        }

        @Override
        public String brake() {
            return super.brake();
        }

    }

}
