package com.finalkeyword.password;

public class SomeClass {
    private static int classCounter = 0;
    public final int instanceNumber;
    private final String name;

    public int getInstanceNumber() {
        return instanceNumber;
    }

    public SomeClass(String name) {
        this.name = name;
        classCounter++;
        instanceNumber = classCounter;
        System.out.println(name + " created, instance is " + instanceNumber);
    }
}
