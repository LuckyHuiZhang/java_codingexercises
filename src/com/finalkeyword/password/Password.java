package com.finalkeyword.password;

public class Password {
    //use keyword static, final to define a constant variable
    private static final int key = 12345678;
    private final int encryptedPassword;

    public Password(int password) {
        this.encryptedPassword = encryptedPassword(password);
    }

    //Use Exclusive or(XOR) to protect the password(defined as 8 digits number)
    private int encryptedPassword(int password) {
        return password ^ key;
    }

    //method define keyword final to match the variable, and its will be auto called, but can not override
    public final void storePassword() {
        System.out.println("Saving password as " + this.encryptedPassword);
    }

    public boolean letMeIn(int password) {
        if (encryptedPassword(password) == this.encryptedPassword) {
            System.out.println("Welcome!");
            return true;
        } else {
            System.out.println("Nope, you cannot come in!");
            return false;
        }
    }
}
