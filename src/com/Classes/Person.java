package com.Classes;

public class Person {
    public static void main(String[] args) {
        Person p1 = new Person();
        p1.setFirstName("lucky");
        p1.setLastName("zhang");
        p1.setAge(18);
//        System.out.println("Full Name: " + p1.getFullName() +  "\n" +
//                "Age: "  + p1.getAge() +  "\n" +
//                "Is Teen: " + p1.isTeen());
        System.out.println(p1.toString());

    }
    private String firstName;
    private String lastName;
    private int age;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 0 || age > 100)
        {
            this.age = 0;
        }else {
            this.age = age;
        }
    }

    public boolean isTeen(){
        if (12 < age && age < 20){
            return true;
        }else {
            return false;
        }
    }

    public String getFullName(){
        if (firstName.isEmpty() && lastName.isEmpty()){
            return "";
        }else if (firstName.isEmpty()){
            return lastName;
        } else if (lastName.isEmpty()){
            return firstName;
        }
        return firstName + " " +lastName;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }
}
