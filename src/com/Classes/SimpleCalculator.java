package com.Classes;

public class SimpleCalculator {
    public static void main(String[] args) {
                 //********* Simple Calculator ******
        System.out.println("Sum Calculator");
        SimpleCalculator simpleCalculator = new SimpleCalculator();
        simpleCalculator.setFirstNumber(24);
        simpleCalculator.setSecondNumber(5);
        System.out.println("Addiction : " + simpleCalculator.getAdditionResult());
        System.out.println("Subtraction : " + simpleCalculator.getSubtractionResult());
        System.out.println("Multiplication : " + simpleCalculator.getMultiplicationResult());
        System.out.println("Division : " + simpleCalculator.getDivisionResult());

    }

    private double firstNumber;
    private double secondNumber;

    public double getFirstNumber() {
        return firstNumber;
    }

    public double getSecondNumber() {
        return secondNumber;
    }

    public void setFirstNumber(double firstNumber) {
        this.firstNumber = firstNumber;
    }

    public void setSecondNumber(double secondNumber) {
        this.secondNumber = secondNumber;
    }

    public double getAdditionResult() {
        return firstNumber + secondNumber;
    }

    public double getSubtractionResult() {
        return firstNumber - secondNumber;
    }

    public double getMultiplicationResult() {
        return firstNumber * secondNumber;
    }

    public double getDivisionResult() {
        if (secondNumber == 0) {
            return 0;
        } else
            return firstNumber / secondNumber;
    }


}
